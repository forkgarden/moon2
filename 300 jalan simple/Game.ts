let cellAr: Cell[] = [];
let cellMax: number = 100;			//maksimum cell boleh dibuat

let peta: string[] = [
	"XXXXXXXXXX",
	"X        X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X        X",
	"XXXXXXXXXX"
]

const st_idle: number = 1;
const st_jalan: number = 2;

let karakter: Karakter = {
	jalur: [],
	jalurn: 0,
	pos: {
		x: 1,
		y: 1
	},
	status: st_idle
}

let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;
let canvasScaleX: number = 1;
let canvasScaleY: number = 1;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;
let gbrTigan: HTMLImageElement;

window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;
	gbrTigan = document.body.querySelector("img#img-tigan") as HTMLImageElement;

	window.onresize = () => {
		resize();
		render();
	};

	resize();
	render();

	setTimeout(() => {
		resize();
		render();
	}, 100);

	canvas.onclick = (e: MouseEvent) => {

		if (karakter.status != st_idle) return;

		let rect: DOMRect = canvas.getBoundingClientRect();
		let poslx: number = (e.clientX - rect.x) * canvasScaleX;
		let posly: number = ((e.clientY - rect.y) * canvasScaleY);
		let posx: number = Math.floor(poslx / 32);
		let posy: number = Math.floor(posly / 32);

		let hasil: number[][] = pfCariJalan(karakter.pos.x, karakter.pos.y, posx, posy);

		karakter.status = st_jalan;
		karakter.jalur = hasil;
		karakter.jalurn = -1;
	}

	setInterval(() => {
		update();
		render();
	}, 100);
}

function update() {
	if (karakter.status == st_idle) {

	}
	else if (karakter.status == st_jalan) {
		if (karakter.jalurn >= (karakter.jalur.length - 1)) {
			karakter.status = st_idle;
		}
		else {
			karakter.jalurn++;
			karakter.pos.x = karakter.jalur[karakter.jalurn][0];
			karakter.pos.y = karakter.jalur[karakter.jalurn][1];
		}
	}
}

function render(): void {
	bersihkanLayar();
	gambarPeta();
	gambarJalan(karakter.jalur);
	gambarKarakter();
}

function gambarKarakter(): void {
	canvasCtx.drawImage(gbrTigan, karakter.pos.x * 32, karakter.pos.y * 32);
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		canvasCtx.drawImage(gbrBola, item[0] * 32 + 8, item[1] * 32 + 8);
	})
}

function gambarPeta(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				canvasCtx.drawImage(gbrBox, ix * 32, jx * 32);
			}
		}
	}
}

function bersihkanLayar(): void {
	canvasCtx.clearRect(0, 0, 320, 240);
}

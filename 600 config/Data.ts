const PF_CEPAT: number = 1;
const PF_A_STAR: number = 2;

const st_idle: number = 1;
const st_jalan: number = 2;
const gp: number = 352;
const gl: number = 352;

let cellAr: Cell[] = [];
let cellMax: number = 1000;			//maksimum cell boleh dibuat

let pfConfig: pfConfig = {
	mode: PF_A_STAR
}

let peta: string[] = [
	"XXXXXXXXXXX",
	"X         X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X         X",
	"XXXXXXXXXXX"
]

let karakter: Karakter = {
	jalur: [],
	jalurn: 0,
	pindahJml: 2,
	pindahn: 0,
	pos: {
		x: 32,
		y: 32
	},
	status: st_idle
}

let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;
let canvasScaleX: number = 1;
let canvasScaleY: number = 1;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;
let gbrTigan: HTMLImageElement;

let pilihan: HTMLOptionElement;

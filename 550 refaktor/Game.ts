window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;
	gbrTigan = document.body.querySelector("img#img-tigan") as HTMLImageElement;

	window.onresize = () => {
		resize();
		render();
	};

	resize();
	render();

	setTimeout(() => {
		resize();
		render();
	}, 100);

	canvas.onclick = (e: MouseEvent) => {

		if (karakter.status != st_idle) return;

		let rect: DOMRect = canvas.getBoundingClientRect() as DOMRect;
		let poslx: number = (e.clientX - rect.x) * canvasScaleX;
		let posly: number = ((e.clientY - rect.y) * canvasScaleY);
		let posx: number = Math.floor(poslx / 32);
		let posy: number = Math.floor(posly / 32);

		let posGrid: Pos2D = krkPosisiGrid(karakter);

		let hasil: number[][] = pfCariJalan(posGrid.x, posGrid.y, posx, posy);

		karakter.status = st_jalan;
		karakter.jalur = hasil;
		karakter.jalurn = -1;
	}

	setInterval(() => {
		update();
		render();
	}, 100);
}

function update() {
	if (karakter.status == st_idle) {

	}
	else if (karakter.status == st_jalan) {
		if (krkCheckPosisiDiGrid(karakter)) {
			karakter.jalurn++;
			if (karakter.jalurn >= karakter.jalur.length - 1) {
				karakter.status = st_idle;
			}
			else {
				karakter.status = st_jalan;
				karakter.pindahn = 0;
				krkPindahGrid(karakter);
			}
		}
		else {
			krkPindahGrid(karakter);
		}
	}
}

function render(): void {
	bersihkanLayar();
	gambarPeta();
	gambarJalan(karakter.jalur);
	gambarKarakter();
}

function gambarKarakter(): void {
	canvasCtx.drawImage(gbrTigan, karakter.pos.x, karakter.pos.y);
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		canvasCtx.drawImage(gbrBola, item[0] * 32 + 8, item[1] * 32 + 8);
	})
}

function gambarPeta(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				canvasCtx.drawImage(gbrBox, ix * 32, jx * 32);
			}
		}
	}
}

function bersihkanLayar(): void {
	canvasCtx.clearRect(0, 0, gp, gl);
}

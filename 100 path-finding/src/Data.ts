let cellAr: Cell[] = [];
let cellMax: number = 100;			//maksimum cell boleh dibuat

let peta: string[] = [
	"XXXXXXXXXX",
	"X        X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X        X",
	"XXXXXXXXXX"
]

let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;
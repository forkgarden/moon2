window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;

	//Ganti posisi awal dan posisi akhir untuk melihat perubahan hasilnya
	let hasil: number[][] = pfCariJalan(2, 2, 7, 2);
	gambarTembok();
	gambarJalan(hasil);
	console.log(JSON.stringify(hasil));
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		canvasCtx.drawImage(gbrBola, item[0] * 32, item[1] * 32);
	})
}

function gambarTembok(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				canvasCtx.drawImage(gbrBox, ix * 32, jx * 32);
			}
		}
	}
} 
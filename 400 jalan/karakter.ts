function krkCheckPosisiDiGrid(karakter: Karakter): boolean {
	if (karakter.pos.x % 32) return false;
	if (karakter.pos.y % 32) return false;
	return true;
}

function krkPosisiGrid(karakter: Karakter): Pos2D {
	return {
		x: Math.floor(karakter.pos.x / 32),
		y: Math.floor(karakter.pos.y / 32)
	}
}

function krkPindahGrid(karakter: Karakter): void {
	let posAwalX: number;
	let posAwalY: number;
	let posSelanjutnyaX: number;
	let posSelanjutnyaY: number;
	let jarakX: number;
	let jarakY: number;
	let posBaruX: number;
	let posBaruY: number;

	karakter.pindahn++;

	//posisi grid sekarang
	posAwalX = karakter.jalur[karakter.jalurn][0] * 32;
	posAwalY = karakter.jalur[karakter.jalurn][1] * 32;

	//posisi grid target
	posSelanjutnyaX = karakter.jalur[karakter.jalurn + 1][0] * 32
	posSelanjutnyaY = karakter.jalur[karakter.jalurn + 1][1] * 32

	//jarak dari grid sekarang ke target
	jarakX = posSelanjutnyaX - posAwalX;
	jarakY = posSelanjutnyaY - posAwalY;

	//posisi karakter baru
	posBaruX = posAwalX + (karakter.pindahn / karakter.pindahJml) * jarakX;
	posBaruY = posAwalY + (karakter.pindahn / karakter.pindahJml) * jarakY;

	karakter.pos.x = posBaruX;
	karakter.pos.y = posBaruY;
}
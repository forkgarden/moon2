function petaKosong(x: number, y: number): boolean {
	return (peta[y].charAt(x) == " ");
}

function petaPosValid(x: number, y: number): boolean {
	if (x < 0) return false;
	if (y >= peta.length) return false;
	if (x >= peta[y].length) return false;
	return petaKosong(x, y);
}
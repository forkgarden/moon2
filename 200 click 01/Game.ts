let cellAr: Cell[] = [];
let cellMax: number = 100;			//maksimum cell boleh dibuat

let peta: string[] = [
	"XXXXXXXXXX",
	"X        X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X        X",
	"X        X",
	"XXXXXXXXXX"
];


let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;

window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;

	gambarPeta();

	canvas.onclick = (e: MouseEvent) => {
		let posx = Math.floor((e.clientX) / 32);
		let posy = Math.floor((e.clientY) / 32);

		bersihkanLayar();

		let hasil: number[][] = pfCariJalan(1, 1, posx, posy);

		gambarPeta();
		gambarJalan(hasil);
	}
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		canvasCtx.drawImage(gbrBola, item[0] * 32, item[1] * 32);
	})
}

function gambarPeta(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				canvasCtx.drawImage(gbrBox, ix * 32, jx * 32);
			}
		}
	}
}

function bersihkanLayar(): void {
	canvasCtx.clearRect(0, 0, 360, 640);
}

let cellAr: Cell[] = [];
let cellMax: number = 100;			//maksimum cell boleh dibuat

let peta: string[] = [
	"          ",
	"  1      1 ",
	"X        X",
	"X    X   X",
	"X    X   X",
	"X    X   X",
	"X        X",
	"XXXXXXXXXX"
]

const st_idle: number = 1;
const st_jalan: number = 2;

let karakter: Karakter = {
	jalur: {
		n: 0,
		data: []
	},
	langkah: {
		n: 0,
		nMax: 3
	},
	pos: {
		x: 32,
		y: 32
	},
	status: st_idle
}

let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;
let gbrTigan: HTMLImageElement;

window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;
	gbrTigan = document.body.querySelector("img#img-tigan") as HTMLImageElement;

	gambarPeta();

	canvas.onclick = (e: MouseEvent) => {

		if (karakter.status != st_idle) return;

		let posx = Math.floor(e.clientX / 32);
		let posy = Math.floor(e.clientY / 32);
		bersihkanLayar();

		let posGrid: Pos2D = krkPosisiGrid(karakter);
		console.log(posGrid);
		console.log('posx ' + posx + '/posy ' + posy);
		console.log('peta:');
		console.log(peta);

		let hasil: number[][] = pfCariJalan(posGrid.x, posGrid.y, posx, posy);

		karakter.status = st_jalan;
		karakter.jalur.data = hasil;
		karakter.jalur.n = -1;

		gambarPeta();
		gambarJalan(hasil);
	}

	test();

	setInterval(() => {
		update();
		render();
	}, 100);
}

function update() {
	if (karakter.status == st_idle) {

	}
	else if (karakter.status == st_jalan) {
		if (krkCheckPosisiDiGrid(karakter)) {
			if (karakter.jalur.n >= karakter.jalur.data.length - 1) {
				karakter.status = st_idle;
			}
			else {
				karakter.status = st_jalan;
				karakter.langkah.n = 0;
				karakter.jalur.n++;
				krkJalan(karakter);
			}
		}
		else {
			krkJalan(karakter);
		}
	}
}

function render(): void {
	bersihkanLayar();
	gambarPeta();
	gambarJalan(karakter.jalur.data);
	gambarKarakter();
}

function gambarKarakter(): void {
	canvasCtx.drawImage(gbrTigan, karakter.pos.x, karakter.pos.y);
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		canvasCtx.drawImage(gbrBola, item[0] * 32 + 8, item[1] * 32 + 8);
	})
}

function gambarPeta(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				canvasCtx.drawImage(gbrBox, ix * 32, jx * 32);
			}
		}
	}
}

function bersihkanLayar(): void {
	canvasCtx.clearRect(0, 0, 320, 240);
}
